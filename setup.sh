#!/bin/bash

TEMPLCLONE_NAME=template_clone
TEMPLCLONE_GIT=git@gitlab.com:burrito82/project_template_cpp.git
TEMPLCLONE_POSTCHECKOUT="ls -l ./setup.sh"

TPS="TEMPLCLONE"

mkdir -p ThirdParty
pushd ThirdParty
    for TP in $TPS
    do
        TP_NAME="${TP}_NAME"
        TP_NAME="${!TP_NAME}"
        echo "processing $TP_NAME..."

        TP_GIT="${TP}_GIT"
        TP_GIT="${!TP_GIT}"

        TP_POSTCHECKOUT="${TP}_POSTCHECKOUT"
        TP_POSTCHECKOUT="${!TP_POSTCHECKOUT}"

        if [ ! -d "$TP_NAME" ]
        then
            git clone "$TP_GIT" "$TP_NAME"
        fi
        if [ -d "$TP_NAME" ]
        then
            pushd "$TP_NAME"
                git pull --ff-only
                if [ ! -z "$TP_POSTCHECKOUT" ]
                then
                    pwd
                    $TP_POSTCHECKOUT
                fi
            popd
        fi
    done
popd

