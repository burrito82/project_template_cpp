%.o: %.cpp
	g++ --std=c++17 -c -o $@ $^

project_template_cpp: main.o
	g++ --std=c++17 -o $@ $^

clean:
	rm -f *.o project_template_cpp

.PHONY: clean
