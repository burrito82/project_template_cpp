/*
 * Copyright (c) 2019
 *  Somebody
 */
#ifndef MYINTERFACE_MYINTERFACE_H
#define MYINTERFACE_MYINTERFACE_H

#include <iostream>

namespace MyInterface
{

static void AddEndl(std::ostream &os)
{
    os << std::endl;
}

} // namespace MyInterface

#endif // MYINTERFACE_MYINTERFACE_H

