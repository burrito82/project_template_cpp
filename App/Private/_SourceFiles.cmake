set(RelativeDir "./Private")
set(RelativeSourceGroup "Private")
set(SubDirs)

set(PrivateSource
    main.cpp

    _SourceFiles.cmake
)
set(PrivateSource_SourceGroup "${RelativeSourceGroup}")

set(LocalSourceGroupFiles)
foreach(File ${PrivateSource})
    list(APPEND LocalSourceGroupFiles "${RelativeDir}/${File}")
    list(APPEND ProjectPrivateSources "${RelativeDir}/${File}")
endforeach()
source_group(${PrivateSource_SourceGroup} FILES ${LocalSourceGroupFiles})

set(SubPrivateSource "")
foreach(Dir ${SubDirs})
    list(APPEND SubPrivateSource "${RelativeDir}/${Dir}/_SourceFiles.cmake")
endforeach()

foreach(SubDirFile ${SubPrivateSource})
    include(${SubDirFile})
endforeach()

