/*
 * Copyright (c) 2019
 *  Somebody
 */
#include "MyLibrary/MyLibrary.h"
#include "MyInterface/MyInterface.h"

#include <iostream>

int main(int argc, char *argv[])
{
    MyLibrary::PrintCopyright(std::cout);
    MyInterface::AddEndl(std::cout);
    return 0;
}

