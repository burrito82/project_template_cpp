/*
 * Copyright (c) 2019
 *  Somebody
 */
#ifndef MYLIBRARY_MYLIBRARYCONFIG_H
#define MYLIBRARY_MYLIBRARYCONFIG_H

// Windows DLL build
#if defined(WIN32) && !defined(MYLIBRARY_STATIC) 
#pragma warning(disable : 4251 4273 4275)
#ifdef MYLIBRARY_EXPORTS
#define MYLIBRARYAPI __declspec(dllexport)
#else
#define MYLIBRARYAPI __declspec(dllimport)
#endif
#else // no Windows or static build
#define MYLIBRARYAPI
#endif

#endif// MYLIBRARY_MYLIBRARYCONFIG_H

