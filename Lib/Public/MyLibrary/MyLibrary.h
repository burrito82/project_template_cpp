/*
 * Copyright (c) 2019
 *  Somebody
 */
#ifndef MYLIBRARY_MYLIBRARY_H
#define MYLIBRARY_MYLIBRARY_H
#include "MyLibraryConfig.h"

#include <iosfwd>

namespace MyLibrary
{

MYLIBRARYAPI void PrintCopyright(std::ostream &os);

} // namespace MyLibrary

#endif // MYLIBRARY_MYLIBRARY_H

