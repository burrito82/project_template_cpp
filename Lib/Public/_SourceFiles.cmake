set(RelativeDir "./Public")
set(RelativeSourceGroup "Public")
set(SubDirs)

set(PublicSource
    ${PROJECT_NAME}/${PROJECT_NAME}.h

    ${PROJECT_NAME}/${PROJECT_NAME}Config.h
    _SourceFiles.cmake
)
set(PublicSource_SourceGroup "${RelativeSourceGroup}")

set(LocalSourceGroupFiles)
foreach(File ${PublicSource})
    list(APPEND LocalSourceGroupFiles "${RelativeDir}/${File}")
    list(APPEND ProjectPublicSources "${RelativeDir}/${File}")
    if(${File} MATCHES ".h$")
        set(PublicExports "${RelativeDir}/${File}\;${PublicExports}")
    endif()
endforeach()
source_group(${PublicSource_SourceGroup} FILES ${LocalSourceGroupFiles})

set(SubPublicSource "")
foreach(Dir ${SubDirs})
    list(APPEND SubPublicSource "${RelativeDir}/${Dir}/_SourceFiles.cmake")
endforeach()

foreach(SubDirFile ${SubPublicSource})
    include(${SubDirFile})
endforeach()

