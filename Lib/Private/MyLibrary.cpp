/*
 * Copyright (c) 2019
 *  Somebody
 */
#include "MyLibrary/MyLibrary.h"

#include <iostream>

namespace MyLibrary
{

char const copyright[] =
"Copyright (c) 2019\n\tSomebody.  All rights reserved.\n\n";

void PrintCopyright(std::ostream &os)
{
    os << copyright;
}

} // namespace MyLibrary

