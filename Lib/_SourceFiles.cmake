set(SubDirs)

file(GLOB_RECURSE PublicSource
    ${PROJECT_SOURCE_DIR}/Public/*.cpp
    ${PROJECT_SOURCE_DIR}/Public/*.h)
set(RelativeDir "./Public")
set(RelativeSourceGroup "Public")
set(PublicSource_SourceGroup "${RelativeSourceGroup}")
set(LocalSourceGroupFiles)
foreach(File ${PublicSource})
    file(RELATIVE_PATH File ${PROJECT_SOURCE_DIR} ${File})
    list(APPEND LocalSourceGroupFiles "${File}")
    list(APPEND ProjectPublicSources "${File}")
    if(${File} MATCHES ".h$")
        set(PublicExports "${RelativeDir}/${File}\;${PublicExports}")
    endif()
endforeach()
source_group(${PublicSource_SourceGroup} FILES ${LocalSourceGroupFiles})

file(GLOB_RECURSE PrivateSource
    ${PROJECT_SOURCE_DIR}/Private/*.cpp
    ${PROJECT_SOURCE_DIR}/Private/*.h)
set(RelativeDir "./Private")
set(RelativeSourceGroup "Private")
set(PrivateSource_SourceGroup "${RelativeSourceGroup}")
set(LocalSourceGroupFiles)
foreach(File ${PrivateSource})
    file(RELATIVE_PATH File ${PROJECT_SOURCE_DIR} ${File})
    list(APPEND LocalSourceGroupFiles "${File}")
    list(APPEND ProjectPrivateSources "${File}")
endforeach()
source_group(${PrivateSource_SourceGroup} FILES ${LocalSourceGroupFiles})

set(SubPublicSource "")
foreach(Dir ${SubDirs})
    list(APPEND SubPublicSource "${RelativeDir}/${Dir}/_SourceFiles.cmake")
endforeach()

foreach(SubDirFile ${SubPublicSource})
    include(${SubDirFile})
endforeach()

